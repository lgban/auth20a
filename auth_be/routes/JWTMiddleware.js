var express = require('express');
var jwt = require('jsonwebtoken');

const secret = "Just keep guessing";

var JWTMiddleware = express.Router();

JWTMiddleware.use((req, res, next) => {
  console.log(req.headers);
  const token = req.headers['authorization'] ?
        req.headers['authorization'].slice(7) : null;

  if (token) {
    jwt.verify(token, secret, (err, decoded) => {
      if (err) {
        return res.status(401).send('Authentication failed');
      } else {
        console.log(decoded);
        req.decoded = decoded;
        next();
      }
    });
  } else {
    res.send('Authentication failed');
  }
});

module.exports = JWTMiddleware;
