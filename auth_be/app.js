var express = require('express');
var logger = require('morgan');
var cors = require('cors');
var jwt = require('jsonwebtoken');

var app = express();

var usersRouter = require('./routes/users');

app.use(logger('dev'));
app.use(cors());
app.use(express.json());

app.use('/users', usersRouter);

const secret = "Just keep guessing";

app.post('/api/auth', (req, res) => {
  const {username,password} = req.body;
  if (username === 'frodo' && password === 'clave') {
    const token = jwt.sign({username: username}, secret, {expiresIn: 1440});
    res.setHeader("authorization", `Bearer ${token}`);
    res.json({data: {token: token}});
  } else {
    res.status(401).send('Authentication failed!');
  }
});

module.exports = app;