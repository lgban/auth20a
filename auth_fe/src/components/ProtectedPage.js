import React, {useEffect, useContext} from 'react';
import {RootContext} from './RootContext';

function ProtectedPage() {
  const {token} = useContext(RootContext);

  useEffect(
    () => {
      fetch("http://localhost:3000/users", {
        method: "GET",
        headers: {
          'authorization': `Bearer ${token}`
        },
      }).then(response => response.json()
      ).then(json => console.log(json));
    },
    [token]
  );

  return (<div>protected</div>);
}

export default ProtectedPage;