import React, { useEffect, useState } from 'react';

export const RootContext = React.createContext();

function Root({ children }) {
  const prevAuth = window.localStorage.getItem('authenticated') || false;
  const prevToken = window.localStorage.getItem('token') || null;
  const [authenticated, setAuthenticated] = useState(prevAuth);
  const [token, setToken] = useState(prevToken);

  useEffect(
    () => {
      window.localStorage.setItem('authenticated', authenticated);
      window.localStorage.setItem('token', token);
    },
    [authenticated, token]
  );

  return (
    <RootContext.Provider value={{
        authenticated,
        setAuthenticated,
        token,
        setToken
    }}>
      {children}
    </RootContext.Provider>
  );
};

export default Root;