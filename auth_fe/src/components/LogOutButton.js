import React, {useContext} from 'react';
import {RootContext} from './RootContext';

function LogOutButton() {
  const {authenticated, setAuthenticated} = useContext(RootContext);
  return authenticated ? (
    <p>
      Welcome!{" "}
      <button onClick={() => setAuthenticated(false)}>
        Log out
      </button>
    </p>
  ) : null;
}

export default LogOutButton;
