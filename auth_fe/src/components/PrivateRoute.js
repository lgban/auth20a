import React, {useContext} from 'react';
import {Route, Redirect} from 'react-router-dom';
import {RootContext} from './RootContext';

function PrivateRoute({ children, ...routeProps }) {
  const { authenticated } = useContext(RootContext);

  return (
    <Route
      {...routeProps}
      render={
        ({ location }) => (
            authenticated ? (
              children
            ) : (
              <Redirect
                to={{
                  pathname: '/login',
                  state: { from: location }
                }}
              />
            )
        )
      }
  />
  );
}

export default PrivateRoute;