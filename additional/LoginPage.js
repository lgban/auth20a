import React, {useContext} from 'react';
import {useHistory, useLocation} from 'react-router-dom';
import {RootContext} from './Root';

function LoginPage() {
  const history = useHistory();
  const location = useLocation();
  const {setAuthenticated,setToken} = useContext(RootContext);

  let { from } = location.state || { from: { pathname: "/" } };

  let handleAuthentication = async () => {
    fetch("http://localhost:3000/api/auth", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          username: "frodo",
          password: "clave"
      })
    }).then( response => response.json()
    ).then( json => {
      console.log( json.data );
      setAuthenticated(true);
      setToken( json.data.token );
      history.replace(from);
    });
  }
    return (
      <div>
        <p>You must log in to view the page at {from.pathname}</p>
        <button onClick={() => handleAuthentication()}>Log in</button>
      </div>
    );
}

export default LoginPage;
