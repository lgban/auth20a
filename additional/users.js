var express = require('express');
var router = express.Router();
const JWTMiddleware = require('./JWTMiddleware');

router.get('/', JWTMiddleware, function(req, res, next) {
  res.json({msg: `Hello ${req.decoded.username}`});
});
  
module.exports = router;
